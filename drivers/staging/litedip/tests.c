/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "tests/tests.h"

#include <linux/device.h>
#include <linux/err.h>

int regmap_mocked_reg_write(void *context, unsigned int reg, unsigned int val)
{
	struct litedip_test_instance *ti = (struct litedip_test_instance *) context;

	KUNIT_EXPECT_EQ(ti->kunit_test, reg % 4, 0);

	ti->bar0[reg / 4] = val;

	return 0;
}

int regmap_mocked_reg_read(void *context, unsigned int reg, unsigned int *val)
{
	struct litedip_test_instance *ti = (struct litedip_test_instance *) context;

	KUNIT_EXPECT_EQ(ti->kunit_test, reg % 4, 0);

	*val = ti->bar0[reg / 4];

	return 0;
}

struct regmap_bus regmap_mocked_bus = {
	.fast_io = 1,
	.reg_write = regmap_mocked_reg_write,
	.reg_read = regmap_mocked_reg_read,
	.reg_format_endian_default = REGMAP_ENDIAN_NATIVE,
	.val_format_endian_default = REGMAP_ENDIAN_NATIVE,
};

static const struct regmap_config regmap_mocked_config = {
	.name = "litedip_mocked",
	.reg_bits = 32,
	.val_bits = 32,
	.reg_stride = 4,

	/* we currently do not need any sort of locking */
	.disable_locking = 1,
	.fast_io = 1,
};

static struct class *litedip_class = NULL;
static struct device *test_dev = NULL;

static int litedip_test_init(struct kunit *test)
{
	struct litedip_test_instance *instance = NULL;
	int err = 0;

	kunit_info(test, "initializing\n");

	instance = kzalloc(sizeof(struct litedip_test_instance), GFP_KERNEL);
	if (!instance)
		return -ENOMEM;

	/* Create a class and device, and don't mind leaking them */
	if (!litedip_class) {
		litedip_class = class_create(THIS_MODULE, "litedip");
		if (IS_ERR(litedip_class)) {
			err = PTR_ERR(litedip_class);
			goto error;
		}
	}
	if (!test_dev) {
		test_dev = device_create(litedip_class, NULL, 0, NULL, "litediptest");
		if (!test_dev) {
			err = PTR_ERR(litedip_class);
			goto error;
		}
	}

	instance->kunit_test = test;

	instance->mock_bus.regs = devm_regmap_init(test_dev, &regmap_mocked_bus,
	                                           instance,
	                                           &regmap_mocked_config);
	KUNIT_EXPECT_PTR_NE(test, instance->mock_bus.regs, NULL);

	test->priv = instance;

	return 0;

error:
	if (!instance)
		kfree(instance);

	return err;
}

static void litedip_test_exit(struct kunit *test)
{

}

static struct kunit_suite litedip_test_suite = {
	.name = "litedip",
	.init = litedip_test_init,
	.exit = litedip_test_exit,
	.test_cases = (struct kunit_case*) litedip_test_cases,
};

/*
 * This registers the above test suite telling KUnit that this is a suite of
 * tests that need to be run.
 */
kunit_test_suites(&litedip_test_suite);

MODULE_LICENSE("Dual MIT/GPL");
