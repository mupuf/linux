/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef __LITEDIP_CORE_H__
#define __LITEDIP_CORE_H__

#include <linux/types.h>
#include <linux/regmap.h>

struct litedip_bus {
	struct regmap *regs;

	size_t (*strlcpy)(struct litedip_bus *bus, uint32_t addr, char *dst,
	                  size_t size);

	/* TODO: add support for DMA */
};

typedef struct litedip_dev litedip_dev;
struct litedip_module {
	struct litedip_dev *dev;

	uint32_t tag;
	char  tag_str[5];
	uint32_t base_addr;

	uint16_t version;
	uint16_t backward_compat;

	int (*init)(struct litedip_module *mod);
	void (*fini)(struct litedip_module *mod);

	/* Bus-related helper functions */
	uint32_t (*rd_u32)(struct litedip_module *mod, uint32_t reg);
	void (*wr_u32)(struct litedip_module *mod, uint32_t reg, uint32_t value);
	void (*mask_u32)(struct litedip_module *mod, uint32_t reg,
	                 uint32_t mask, uint32_t val);
};
#define LITEDIP_MOD_RD32(m, reg) (m->mod.rd_u32(&m->mod, reg))
#define LITEDIP_MOD_WR32(m, reg, val) (m->mod.wr_u32(&m->mod, reg, val))
#define LITEDIP_MOD_MASK32(m, reg, mask, val) (m->mod.mask_u32(&m->mod, reg, mask, val))

struct litedip_dev {
	struct litedip_bus *bus;

	struct litedip_module *top_level;
};

int litedip_init(struct litedip_dev *dev, struct litedip_bus* bus);
void litedip_fini(struct litedip_dev *);

#endif
