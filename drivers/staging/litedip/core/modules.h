/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef MODULES_H
#define MODULES_H

#include <linux/types.h>

#include "core.h"

struct litedip_module_def {
	uint32_t tag;
	uint16_t version;

	uint32_t alloc_size;
	int (*init)(struct litedip_module *mod);
	void (*fini)(struct litedip_module *mod);
};

/* For kunit testing only */
struct litedip_module *
    __litedip_create_module(struct litedip_dev *dev,
                            struct litedip_module_def **modules,
                            size_t modules_len, uint32_t tag,
                            uint32_t base_addr, uint16_t version,
                            uint16_t backward_compat);

struct litedip_module *
                litedip_create_module(struct litedip_dev *dev, uint32_t tag,
                                      uint32_t base_addr, uint16_t version,
                                      uint16_t backward_compat);

struct litedip_module *
                litedip_find_module(struct litedip_dev *dev, uint32_t tag);

#endif // MODULES_H
