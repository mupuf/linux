/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <linux/errno.h>
#include <linux/string.h>
#include <linux/printk.h>
#include <linux/slab.h>
#include <linux/compiler.h>
#include <linux/regmap.h>

#include <asm-generic/bug.h>

#include "modules.h"

#include "../toplevel/toplevel.h"

struct litedip_module_def *available_modules[] = {
	/* TOP LEVEL */
	&toplevel_v1,

	/* Put modules here */

	/* End of the list */
};

static uint32_t rd_u32(struct litedip_module *mod, uint32_t reg)
{
	struct litedip_bus *bus = mod->dev->bus;
	uint32_t val;

	regmap_read(bus->regs, mod->base_addr + reg, &val);
	return val;
}

static void wr_u32(struct litedip_module *mod, uint32_t reg, uint32_t value)
{
	struct litedip_bus *bus = mod->dev->bus;
	regmap_write(bus->regs, mod->base_addr + reg, value);
}

static void mask_u32(struct litedip_module *mod, uint32_t reg,
                     uint32_t mask, uint32_t value)
{
	struct litedip_bus *bus = mod->dev->bus;
	regmap_update_bits(bus->regs, mod->base_addr + reg, mask, value);
}

struct litedip_module *
    __litedip_create_module(struct litedip_dev *dev,
                            struct litedip_module_def **modules,
                            size_t modules_len, uint32_t tag,
                            uint32_t base_addr, uint16_t version,
                            uint16_t backward_compat)
{
	struct litedip_module_def *best_version = NULL;
	struct litedip_module_def *mod_def = NULL;
	struct litedip_module *mod;
	char tag_str[5];
	int i;

	if (version == 0 || backward_compat == 0)
		return NULL;

	/* compute the name of the module */
	tag_str[0] = (tag >> 24) & 0xff;
	tag_str[1] = (tag >> 16) & 0xff;
	tag_str[2] = (tag >> 8) & 0xff;
	tag_str[3] = (tag >> 0) & 0xff;
	tag_str[4] = '\0';

	for (i = 0; i < modules_len; i++) {
		mod_def = modules[i];

		if (mod_def->tag != tag)
			continue;

		if (!best_version || (mod_def->version > best_version->version &&
		                      mod_def->version <= version))
			best_version = mod_def;

		if (mod_def->version == version)
			break;
	};

	if (best_version == NULL) {
		printk(KERN_WARNING "No drivers found for module %s (0x%x)\n",
		       tag_str, tag);
		return NULL;
	}

	if (!best_version || best_version->version < backward_compat)
		return NULL;

	/* We found the most-suitable module, allocate storage for it */
	mod = kzalloc(mod_def->alloc_size, GFP_KERNEL);
	if (!mod)
		return NULL;

	mod->dev = dev;
	mod->tag = tag;
	strcpy(mod->tag_str, tag_str);
	mod->base_addr = base_addr;
	mod->version = version;
	mod->backward_compat = backward_compat;
	mod->init = best_version->init;
	mod->fini = best_version->fini;

	/* Helpers */
	mod->rd_u32 = rd_u32;
	mod->wr_u32 = wr_u32;
	mod->mask_u32 = mask_u32;

	best_version->init(mod);

	return mod;
}

struct litedip_module *
    litedip_create_module(struct litedip_dev *dev, uint32_t tag,
                          uint32_t base_addr, uint16_t version,
                          uint16_t backward_compat)
{
	return __litedip_create_module(dev, available_modules,
	                               ARRAY_SIZE(available_modules),
	                               tag, base_addr, version, backward_compat);
}

struct litedip_module *
    litedip_find_module(struct litedip_dev *dev, uint32_t tag)
{
	struct litedip_mod_toplevel *toplevel = litedip_mod_toplevel(dev->top_level);
	struct litedip_module *mod = NULL;
	int i;

	for (i = 0; i < toplevel->mod_count; i++) {
		mod = toplevel->modules[i];
		if (mod == NULL)
			return NULL;

		if (mod->tag == tag)
			return mod;
	}

	return NULL;
}
