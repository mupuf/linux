/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "core.h"
#include "modules.h"

#include "../toplevel/toplevel.h"

#include <linux/slab.h>
#include <linux/printk.h>
#include <linux/regmap.h>

static const struct reg_field field_version = {.reg = 0x8, .lsb = 0, .msb = 7};
static const struct reg_field field_backward_compat = {.reg = 0x8, .lsb = 8, .msb = 15};

int litedip_init(struct litedip_dev *dev, struct litedip_bus* bus)
{
	struct regmap_field *f_version =
	                regmap_field_alloc(bus->regs, field_version);
	struct regmap_field *f_backward_compat =
	                regmap_field_alloc(bus->regs, field_backward_compat);
	unsigned int header_high, header_low, tl_version = 0, tl_backward_compat = 0;

	/* Check that the device is indeed a litedip-compatible device */
	regmap_read(bus->regs, 0x0, &header_high);
	regmap_read(bus->regs, 0x4, &header_low);

	if (header_high != 0x4C495445 || header_low != 0x44495000)
		return -EINVAL;

	regmap_field_read(f_version, &tl_version);
	regmap_field_read(f_backward_compat, &tl_backward_compat);

	/* Create the device */
	dev->bus = bus;
	dev->top_level = litedip_create_module(dev, LITEDIP_MOD_TOPL, 0,
	                                       tl_version, tl_backward_compat);
	if (!dev->top_level) {
		printk(KERN_CRIT "Failed to allocate a driver for the top level module version %i (backwards compatible to v%i)\n",
		       tl_version, tl_backward_compat);
		return -EINVAL;
	}

	return 0;
}

void litedip_fini(struct litedip_dev *dev)
{
	dev->top_level->fini(dev->top_level);
}
