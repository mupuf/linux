#ifndef LITEDIP_TESTS_H
#define LITEDIP_TESTS_H

#include <kunit/test.h>

#include "../core/core.h"

struct litedip_test_instance {
	struct kunit *kunit_test;

	struct litedip_bus mock_bus;
	uint32_t *bar0;
};

/* Helpers */

/* Test cases */
void core_toplevel_invalid_version(struct kunit *test);
void core_toplevel_empty(struct kunit *test);
void core_module_create(struct kunit *test);
void core_module_find(struct kunit *test);

static const struct kunit_case litedip_test_cases[] = {
	KUNIT_CASE(core_toplevel_invalid_version),
	KUNIT_CASE(core_toplevel_empty),
	KUNIT_CASE(core_module_create),
	KUNIT_CASE(core_module_find),
	{}
};

#endif // LITEDIP_TESTS_H
