/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "tests.h"

#include "../toplevel/toplevel.h"

/* core.c */
void core_toplevel_invalid_version(struct kunit *test)
{
	struct litedip_test_instance *ti = (struct litedip_test_instance *) test->priv;
	struct litedip_dev dev;

	/* Set up the mocked address space */
	uint32_t bar0[] = {
		0x4C495445, 0x44495000, /* LITEDIP header */
		0x00000000 /* version = 0, backward compat = 0, mod_count = 0 */
	};
	ti->bar0 = bar0;

	KUNIT_EXPECT_EQ(test, litedip_init(&dev, &ti->mock_bus), -EINVAL);
}

void core_toplevel_empty(struct kunit *test)
{
	struct litedip_test_instance *ti = (struct litedip_test_instance *) test->priv;
	struct litedip_mod_toplevel *toplevel;
	struct litedip_dev dev;

	uint32_t bar0[] = {
		0x4C495445, 0x44495000, /* LITEDIP header */
		0x00000101 /* version = 0, backward compat = 0, mod_count = 0 */
	};
	ti->bar0 = bar0;

	KUNIT_EXPECT_EQ(test, litedip_init(&dev, &ti->mock_bus), 0);
	toplevel = litedip_mod_toplevel(dev.top_level);

	KUNIT_EXPECT_TRUE(test, toplevel->mod.version == 1);
	KUNIT_EXPECT_TRUE(test, toplevel->mod.backward_compat == 1);
	KUNIT_EXPECT_TRUE(test, toplevel->mod_count == 0);
}

/* modules.c */
#define FANC_TAG 0x46414e43
#define SENS_TAG 0x53454e53

struct litedip_module_test {
	struct litedip_module mod;

	int check;
};

int mod_init(struct litedip_module *mod)
{
	struct litedip_module_test *test_mod = (struct litedip_module_test *)mod;
	test_mod->check = 42;
	return 0;
}

void fanc_fini_v1(struct litedip_module *mod) {}
void fanc_fini_v5(struct litedip_module *mod) {}
void sens_fini_v1(struct litedip_module *mod) {}
void sens_fini_v3(struct litedip_module *mod) {}

static void __check_module_create(struct kunit *test, struct litedip_dev *dev,
                                  struct litedip_module_def **modules,
                                  size_t modules_len, uint32_t tag,
                                  const char *tag_str, uint32_t base_addr,
                                  uint16_t version, uint16_t backward_compat,
                                  struct litedip_module_def *expected)
{
	struct litedip_module_test *mod_test;
	struct litedip_module *mod;

	mod = __litedip_create_module(dev, modules, modules_len, tag, base_addr,
	                              version, backward_compat);
	mod_test = (struct litedip_module_test *)mod;

	if (!expected)
		KUNIT_EXPECT_PTR_EQ(test, (void *)mod, NULL);
	else {
		KUNIT_EXPECT_PTR_EQ(test, mod->dev, dev);
		KUNIT_EXPECT_EQ(test, mod->tag, tag);
		KUNIT_EXPECT_TRUE(test, strcmp(mod->tag_str, tag_str) == 0);
		KUNIT_EXPECT_EQ(test, mod->base_addr, base_addr);
		KUNIT_EXPECT_EQ(test, mod->version, version);
		KUNIT_EXPECT_EQ(test, mod->backward_compat, backward_compat);

		KUNIT_EXPECT_PTR_EQ(test, mod->init, expected->init);
		KUNIT_EXPECT_PTR_EQ(test, mod->fini, expected->fini);

		KUNIT_EXPECT_EQ(test, mod_test->check, 42);

		/* Check the helpers */
		mod->wr_u32(mod, 0, 0x1337);
		KUNIT_EXPECT_EQ(test, mod->rd_u32(mod, 0), (uint32_t) 0x1337);
		mod->mask_u32(mod, 0, 0xFF00, 0xFFFF);
		KUNIT_EXPECT_EQ(test, mod->rd_u32(mod, 0), (uint32_t) 0xFF37);
	}
}

void core_module_create(struct kunit *test)
{
	struct litedip_test_instance *ti = (struct litedip_test_instance *) test->priv;

	struct litedip_module_def fanc_v1 = {
		.tag = FANC_TAG,
		.version = 1,
		.alloc_size = sizeof(struct litedip_module_test),
		.init = mod_init,
		.fini = fanc_fini_v1
	};

	struct litedip_module_def fanc_v5 = {
		.tag = FANC_TAG,
		.version = 5,
		.alloc_size = sizeof(struct litedip_module_test),
		.init = mod_init,
		.fini = fanc_fini_v5
	};

	struct litedip_module_def sens_v1 = {
		.tag = SENS_TAG,
		.version = 1,
		.alloc_size = sizeof(struct litedip_module_test),
		.init = mod_init,
		.fini = sens_fini_v1
	};

	struct litedip_module_def sens_v3 = {
		.tag = SENS_TAG,
		.version = 3,
		.alloc_size = sizeof(struct litedip_module_test),
		.init = mod_init,
		.fini = sens_fini_v3
	};

	struct litedip_module_def *mods_avail[] = {
		&fanc_v1, &fanc_v5,
		&sens_v1, &sens_v3
	};

	/* Create a simple device with just a bus */
	struct litedip_dev dev;
	uint32_t bar0[] = {
		0x4C495445, 0x44495000, /* LITEDIP header */
		0x00000000, /* version = 0, backward compat = 0, mod_count = 0 */
		0x00000000, 0x00000000
	};
	ti->bar0 = bar0;
	dev.bus = &ti->mock_bus;

	/* Check that the values at the base addresses of the modules are 0 */
	KUNIT_EXPECT_EQ(test, bar0[3], (uint32_t) 0);
	KUNIT_EXPECT_EQ(test, bar0[4], (uint32_t) 0);

	/* FANC */
	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      FANC_TAG, "FANC", 0xC, 1, 1, &fanc_v1);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      FANC_TAG, "FANC", 0xC, 3, 1, &fanc_v1);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      FANC_TAG, "FANC", 0xC, 3, 2, NULL);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      FANC_TAG, "FANC", 0xC, 5, 2, &fanc_v5);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      FANC_TAG, "FANC", 0xC, 5, 5, &fanc_v5);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      FANC_TAG, "FANC", 0xC, 6, 5, &fanc_v5);

	/* Check that only the FANC module got affected by the writes */
	KUNIT_EXPECT_NE(test, bar0[3], (uint32_t) 0);
	KUNIT_EXPECT_EQ(test, bar0[4], (uint32_t) 0);

	/* SENS */
	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      SENS_TAG, "SENS", 0x10, 1, 1, &sens_v1);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      SENS_TAG, "SENS", 0x10, 2, 1, &sens_v1);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      SENS_TAG, "SENS", 0x10, 2, 2, NULL);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      SENS_TAG, "SENS", 0x10, 3, 2, &sens_v3);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      SENS_TAG, "SENS", 0x10, 3, 3, &sens_v3);

	__check_module_create(test, &dev, mods_avail, ARRAY_SIZE(mods_avail),
	                      SENS_TAG, "SENS", 0x10, 4, 3, &sens_v3);

	/* Check that only the SENS module got affected by the writes */
	KUNIT_EXPECT_NE(test, bar0[3], (uint32_t) 0);
	KUNIT_EXPECT_NE(test, bar0[4], (uint32_t) 0);
}

void core_module_find(struct kunit *test)
{
	struct litedip_test_instance *ti = (struct litedip_test_instance *) test->priv;

	struct litedip_module mod_fanc = { .tag = FANC_TAG };
	struct litedip_module mod_sens = { .tag = SENS_TAG };
	struct litedip_module *modules[] = { &mod_fanc, &mod_sens };
	struct litedip_mod_toplevel top_level = {
		.mod_count = ARRAY_SIZE(modules),
		.modules = modules
	};

	struct litedip_dev dev = {
		.bus = &ti->mock_bus,
		.top_level = &top_level.mod
	};

	KUNIT_EXPECT_PTR_EQ(test, litedip_find_module(&dev, FANC_TAG), &mod_fanc);
	KUNIT_EXPECT_PTR_EQ(test, litedip_find_module(&dev, SENS_TAG), &mod_sens);
	KUNIT_EXPECT_PTR_EQ(test, (void *)litedip_find_module(&dev, 0x1234), NULL);
}
