/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/printk.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/types.h>
#include <linux/err.h>
#include <asm-generic/io.h>

#include "litedip_drv.h"

MODULE_LICENSE("Dual MIT/GPL");
MODULE_AUTHOR("Martin Peres <mupuf@x.org>");
MODULE_DESCRIPTION("Generic driver for all LiteDIP-based devices");

/* ----------------- PCI ------------------ */

/* C-style inheritance is achieved by extending the original struct with
 * additional fields after it. Conversion between types is achieved by simply
 * casting the pointer from one type to another.
 */
struct litedip_bus_pci {
	struct litedip_bus bus;

	struct pci_dev *pdev;
	const struct pci_device_id *ent;
	void __iomem *bar0;
};

struct litedip_bus_pci *litedip_bus_pci(struct litedip_bus *bus)
{
	return (struct litedip_bus_pci *) bus;
}

size_t litedip_strlcpy(struct litedip_bus *bus, uint32_t addr, char *dst, size_t size)
{
	uint32_t val, start_addr = addr & 0xFFFFFFFC;
	size_t start_offset = addr - start_addr;
	size_t off = 0, i;
	char c;

	/* This looks a little complicate because we cannot start reading
	 * at the provided address, but instead need to make 32-bit-aligned
	 * reads, which forces us to discard the first characters.
	 */
	addr = start_addr;
	do {
		regmap_read(bus->regs, addr, &val);

		for (i = 0; i < 4 && off < size - 1; i++) {
			c = (val >> ((3 - i) * 8)) & 0xFF;
			if (off > 0 || i >= start_offset) {
				dst[off++] = c;
				if (c == '\0')
					break;
			}
		}

		addr += 4;
	} while(c != '\0' && off < size - 1);

	/* Make sure the last character is a NULL character */
	dst[off] = '\0';

	return off;
}

static const struct regmap_config pci_regmap_cfg = {
	.name = "litedip_pci",
	.reg_bits = 32,
	.val_bits = 32,
	.reg_stride = 4,

	/* we currently do not need any sort of locking */
	.disable_locking = 1,
	.fast_io = 1,
};

static struct litedip_bus *
                litedip_pci_bus_create(struct pci_dev *pdev,
                                       const struct pci_device_id *ent)
{
	struct litedip_bus_pci *bus_pci;
	struct litedip_bus *bus;
	void __iomem *bar0;

	bar0 = pci_iomap(pdev, 0, 0);
	if (!bar0) {
		printk(KERN_ERR "failed to iomap bar0.\n");
		return NULL;
	}

	bus_pci = kzalloc(sizeof(struct litedip_bus_pci), GFP_KERNEL);
	if (!bus_pci)
		return NULL;

	bus_pci->bus.regs = devm_regmap_init_mmio(&pdev->dev, bar0, &pci_regmap_cfg);
	if (IS_ERR(bus_pci->bus.regs)) {
		int ret = PTR_ERR(bus_pci->bus.regs);
		printk(KERN_ERR "Failed to create the bus: %d\n", ret);
		return ERR_PTR(ret);
	}

	bus = &bus_pci->bus;
	bus->strlcpy = litedip_strlcpy;
	bus_pci->pdev = pdev;
	bus_pci->ent = ent;
	bus_pci->bar0 = bar0;

	return bus;
}

static void litedip_pci_bus_free(struct litedip_bus *bus)
{
	struct litedip_bus_pci *pci_bus = litedip_bus_pci(bus);
	pci_iounmap(pci_bus->pdev, pci_bus->bar0);
}

static int litedip_pci_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
	struct litedip_linux_dev *l_dev;
	struct litedip_bus *bus;
	int ret;

	/* Create a bus object for litedip to access the device */
	bus = litedip_pci_bus_create(pdev, ent);
	if (!bus)
		return -ENOMEM;

	/* Allocate the OS-specific data */
	l_dev = kzalloc(sizeof(struct litedip_linux_dev), GFP_KERNEL);
	if (!l_dev) {
		litedip_pci_bus_free(bus);
		return -ENOMEM;
	}

	l_dev->device = &pdev->dev;

	ret = litedip_init(&l_dev->dev, bus);
	if (ret) {
		litedip_pci_bus_free(bus);
		kfree(l_dev);
		return ret;
	}

	pci_set_drvdata(pdev, l_dev);

	return 0;
}

static void litedip_pci_remove(struct pci_dev *pdev)
{
	struct litedip_linux_dev *l_dev;
	struct litedip_bus *bus;

	l_dev = pci_get_drvdata(pdev);
	if (!l_dev)
		return;

	bus = l_dev->dev.bus;

	litedip_fini(&l_dev->dev);

	litedip_pci_bus_free(bus);

	pci_set_drvdata(pdev, NULL);
	kfree(l_dev);
}

static const struct pci_device_id pcidevtbl[] = {
        { 0x10ee, 0x7021, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0 },
        { }
};

static struct pci_driver litedip_pci_driver = {
	.name = "LiteDIP PCI device",
	.id_table = pcidevtbl,
	.probe = litedip_pci_probe,
	.remove = litedip_pci_remove,
};

/* ------------------ Driver ------------------ */

static int __init litedip_drv_init(void)
{
	int rc;

	rc = pci_register_driver(&litedip_pci_driver);
	if (rc) {
		printk(KERN_WARNING "failed to register the driver.\n");
		return rc;
	}

	return 0;
}

static void __exit litedip_drv_exit(void)
{
	pci_unregister_driver(&litedip_pci_driver);
}

module_init(litedip_drv_init);
module_exit(litedip_drv_exit);
