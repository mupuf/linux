/*
 * Copyright 2020 Martin Peres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <linux/slab.h>

#include "toplevel.h"
#include "../core/core.h"

static const struct reg_field module_count = {.reg = 0x8, .lsb = 16, .msb = 24};
static const struct reg_field tag = {.reg = 0xc, .lsb = 0, .msb = 32, .id_size = 255, .id_offset = 12};
static const struct reg_field base_addr = {.reg = 0x10, .lsb = 0, .msb = 32, .id_size = 255, .id_offset = 12};
static const struct reg_field version = {.reg = 0x14, .lsb = 0, .msb = 16, .id_size = 255, .id_offset = 12};
static const struct reg_field backward_compat = {.reg = 0x14, .lsb = 16, .msb = 32, .id_size = 255, .id_offset = 12};

static int litedip_mod_toplevel_init(struct litedip_module *mod)
{
	struct litedip_mod_toplevel *toplevel = litedip_mod_toplevel(mod);
	struct litedip_dev *dev = mod->dev;
	struct litedip_bus* bus = dev->bus;
	struct regmap_field *f_module_count = regmap_field_alloc(bus->regs, module_count);
	const char *err_msg;
	unsigned int mod_count, i;

	regmap_field_read(f_module_count, &mod_count);
	toplevel->mod_count = 0;

	toplevel->modules = kzalloc(sizeof(struct litedip_module *) * mod_count, GFP_KERNEL);
	if (!toplevel->modules) {
		return -ENOMEM;
	}

	/* Go through the entire list of modules */
	err_msg = KERN_WARNING \
	          "Could not find a suitable driver for %x (%s) v%i\n";
	for (i = 0; i < mod_count; i++) {
		struct regmap_field *f_tag = regmap_field_alloc(bus->regs, tag);
		struct regmap_field *f_base_addr = regmap_field_alloc(bus->regs, base_addr);
		struct regmap_field *f_version = regmap_field_alloc(bus->regs, version);
		struct regmap_field *f_backward_compat = regmap_field_alloc(bus->regs, backward_compat);
		unsigned int tag, base_addr, version, backward_compat;
		struct litedip_module *mod;

		/* Parse the module's tag, base address, version, and backward
		 * compatibility version
		 */
		regmap_fields_read(f_tag, i, &tag);
		regmap_fields_read(f_base_addr, i, &base_addr);
		regmap_fields_read(f_version, i, &version);
		regmap_fields_read(f_backward_compat, i, &backward_compat);

		/* Try to find the corresponding module */
		mod = litedip_create_module(dev, tag, base_addr, version,
		                            backward_compat);
		if (mod) {
			toplevel->modules[toplevel->mod_count++] = mod;
		}
	}

	return 0;
}

static void litedip_mod_toplevel_fini(struct litedip_module *mod)
{
	struct litedip_mod_toplevel *toplevel = litedip_mod_toplevel(mod);
	int i;

	if (!mod)
		return;

	if (toplevel->modules) {
		for (i = 0; i < toplevel->mod_count; i++) {
			struct litedip_module *mod = toplevel->modules[i];
			if (mod)
				mod->fini(mod);
		}
		kfree(toplevel->modules);
	}

	kfree(mod);
}

struct litedip_module_def toplevel_v1 = {
	.tag = LITEDIP_MOD_TOPL,
	.version = 1,
	.alloc_size = sizeof(struct litedip_mod_toplevel),
	.init = litedip_mod_toplevel_init,
	.fini = litedip_mod_toplevel_fini,
};
