#ifndef TOPLEVEL_H
#define TOPLEVEL_H

#include "../core/core.h"
#include "../core/modules.h"

#define LITEDIP_MOD_TOPL 0x544f504c  /* TOPL */

struct litedip_mod_toplevel {
	struct litedip_module mod;

	uint8_t mod_count;
	struct litedip_module **modules;
};

struct litedip_mod_toplevel *litedip_mod_toplevel(struct litedip_module *mod);
struct litedip_mod_toplevel *litedip_get_mod_toplevel(struct litedip_dev *dev);

extern struct litedip_module_def toplevel_v1;

#endif // TOPLEVEL_H
